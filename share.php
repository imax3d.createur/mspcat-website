<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

if (isset($_POST['blob'])) {
	if (strpos($_POST['blob'], 'data:image/png;base64') === 1) {
		// Chargement du masque
		$maskPath = 'img/masque.png'; // Assurez-vous que c'est un PNG avec transparence
		$mask = imagecreatefrompng($maskPath);
		
		// Récupération du BLOB
		$data = $_POST['blob'];
		
		// Supprimer le préfixe du type MIME pour obtenir uniquement la chaîne base64
		list(, $data) = explode(';', $data);
		list(, $data) = explode(',', $data);
		$data = base64_decode($data);
		
		// Créer une image à partir des données binaires
		$image = imagecreatefromstring($data);
		if ($image !== false) {
			$nom_final = 'MSPCAT_'.genererChaineAleatoire().'.png';
			$nom_img_temps = 'TEMP_MSPCAT_'.genererChaineAleatoire().'.png';
			
			
			// Traiter l'image ici
			// Par exemple, sauvegarder l'image en tant que fichier PNG
			$filePath = 'MSPCAT/'.$nom_img_temps;
			imagepng($image, $filePath);
		
			// Activer l'alpha blending sur le masque
			imagealphablending($mask, true);
			
			// Obtenir les dimensions de l'image et du masque
			$maskWidth = 868;
			$maskHeight = 603;
			
			$imageWidth = 790;
			$imageHeight = 490;
			
			// Position et taille pour la copie
			//$dest_x = 47; // Ajuster selon vos besoins
			//$dest_y = 35; // Ajuster selon vos besoins
			
			$dest_x = 61; // Ajuster selon vos besoins
			$dest_y = 47; // Ajuster selon vos besoins
			
			// Fusionner l'image avec le masque
			imagecopy($mask, $image, $dest_x, $dest_y, 0, 0, $imageWidth, $imageHeight);
			
			// Enregistrer l'image résultante
			$newImagePath = 'MSPCAT/'.$nom_final;
			imagepng($mask, $newImagePath);
			
			imagedestroy($mask);
			imagedestroy($image);
			unlink('MSPCAT/'.$nom_img_temps);
			
			//imagedestroy($mask);
			
			echo 'MSPCAT/'.$nom_final;
		} else {
			echo "Les données ne sont pas une image valide.";
		}
	}else {
		echo "ERROR1";
	}
}else {
	echo "ERROR2";
}

function genererChaineAleatoire($longueur = 42) {
	$caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$nombreCaracteres = strlen($caracteres);
	$chaineAleatoire = '';

	for ($i = 0; $i < $longueur; $i++) {
		$chaineAleatoire .= $caracteres[rand(0, $nombreCaracteres - 1)];
	}

	return $chaineAleatoire;
}







?>